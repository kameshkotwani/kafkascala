ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
	.settings(
		name := "KafkaScala"
	)

lazy val commonSettings = Seq(
	"org.apache.kafka" %% "kafka" % "3.4.0",
	"org.apache.kafka" % "kafka-clients" % "3.4.0",
	"org.slf4j" % "slf4j-api" % "2.0.7",
	"org.slf4j" % "slf4j-simple" % "2.0.7"
)



lazy val kafka_basics = (project in file("kafka-basics"))
	.settings(
		name := "kafka-basics",
		libraryDependencies++=commonSettings
	)

lazy val wikimedia_project = (project in file ("wikimedia_project"))

	.settings(
		name:="wikimedia_project",
		libraryDependencies++=commonSettings:+"org.apache.kafka" % "kafka-streams" % "3.4.0":+"org.apache.kafka" % "connect-api" % "3.4.0":+
			"com.squareup.okhttp3" % "okhttp" % "4.11.0":+"com.launchdarkly" % "okhttp-eventsource" % "2.5.0"


	)