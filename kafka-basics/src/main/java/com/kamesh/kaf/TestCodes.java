package com.kamesh.kaf;
import org.apache.kafka.common.serialization.StringDeserializer;
public class TestCodes {
    public static void main(String[] args) {
        System.out.println("getName(): " + StringDeserializer.class.getName());
        System.out.println("getSimpleName(): " + StringDeserializer.class.getSimpleName());
        System.out.println("getClass: " + StringDeserializer.class.getClass());
    }
}
