package io.conduktor.kafka

import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord, ConsumerRecords, CooperativeStickyAssignor, KafkaConsumer}
import org.apache.kafka.common.serialization.StringDeserializer
import org.slf4j.LoggerFactory

import java.time.Duration
import java.util
import java.util.Properties


object ConsumerDemoCooperative extends App {
  private val log = LoggerFactory.getLogger(ConsumerDemoCooperative.getClass.getName)

  // creating the properties for the Consumer
  val groupId = "thirdGroup"
  val prop: Properties = new Properties()
  prop.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.56.11:9091")
  prop.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getName)
  prop.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getName)
  prop.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId)
  prop.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
  // The default behaviour is exactly once for a consumer group, so when re-running the code, it will not take messages from beginning
  prop.setProperty(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG,classOf[CooperativeStickyAssignor].getName)



  // Create the Consumer
  val consumer: KafkaConsumer[String, String] = new KafkaConsumer(prop)
  val topics = new util.ArrayList[String]()
  topics.add("myTopic")
  log.info(topics.toString)
  // subscribe to the topic
  consumer.subscribe(topics)

  try {
    //poll for new data

    while (true) {
      log.info("polling")
      val records = consumer.poll(Duration.ofMillis(1000))
      records.forEach {
        r => log.info(s"RECORD: Key: ${r.key()} || value: ${r.value()} ||  Topic: ${r.topic()} || Partition: ${r.partition()}|| Offset: ${r.offset()}\n")
      }
    }
  }
  catch {
    case exp:Exception => log.error(s"$exp")
  }
  finally {}
  consumer.close()

}
