package io.conduktor.kafka

import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord, ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.errors.WakeupException
import org.apache.kafka.common.serialization.StringDeserializer
import org.slf4j.LoggerFactory

import java.time.Duration
import java.util
import java.util.Properties


object ConsumerDemoShutdownHook extends App {

	private val log = LoggerFactory.getLogger(ConsumerDemoShutdownHook.getClass.getName)

	// creating the properties for the Consumer
	val groupId = " "
	val prop: Properties = new Properties()
	prop.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.56.11:9091")
	prop.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getName)
	prop.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getName)
	prop.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId)
	prop.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
	// The default behaviour is exactly once for a consumer group, so when re-running the code, it will not take messages from beginning
	prop.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false")


	// Create the Consumer
	val consumer: KafkaConsumer[String, String] = new KafkaConsumer(prop)
	val topics = new util.ArrayList[String]()
	topics.add("myTopic1")


	// getting the current Thread to shut the consumer down
	private val thisThread: Thread = Thread.currentThread()

	// adding the shutdownHook
	Runtime.getRuntime.addShutdownHook(new Thread() {
		override def run(): Unit = {
			log.info("Detected the shutdown, calling the consumer.wakeup()")
			consumer.wakeup()
			try {
				thisThread.join()
			}
			catch {
				case ipexp: InterruptedException => ipexp.printStackTrace()
			}
		}
	})
	consumer.subscribe(topics)

	try {
		while (true) {
			log.info("polling")
			val records: ConsumerRecords[String, String] = consumer.poll(Duration.ofMillis(1000))
			records.forEach {
				r: ConsumerRecord[String, String] => log.info(s"RECORD: Key: ${r.key()} || value: ${r.value()} ||  Topic: ${r.topic()} || Partition: ${r.partition()}|| Offset: ${r.offset()}\n")
			}
		}
	} catch {
		case wkExp: WakeupException => log.info(wkExp.toString)
		case exp: Exception => exp.printStackTrace()
	}
	finally {
		log.info("closing the consumer")
		consumer.close()

	}

} 
