package io.conduktor.kafka

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.{Logger, LoggerFactory}

import java.util.Properties


object ProducerDemo extends App{
  private val log = LoggerFactory.getLogger(ProducerDemo.getClass.getName)
  private val prop:Properties = new Properties()
  prop.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"192.168.56.11:9091")
  prop.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)
  prop.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)

  private val producer: KafkaProducer[String, String] = new KafkaProducer(prop)
  private val record: ProducerRecord[String, String] = new ProducerRecord[String, String]("myTopic", "Helllo from Scala Producer")
  try {

    producer.send(record)
    producer.flush()
  }
  catch {
    case exp:Exception => log.error(s"$exp")
  }
  finally {
    producer.close()
  }

}
