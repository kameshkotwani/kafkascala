package io.conduktor.kafka

import org.apache.kafka.clients.producer.{Callback, KafkaProducer, ProducerConfig, ProducerRecord, RecordMetadata}
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.{Logger, LoggerFactory}

import java.util.Properties


object ProducerDemoWithCallback extends App {
  private val log = LoggerFactory.getLogger(ProducerDemoWithCallback.getClass.getName)
  private val prop: Properties = new Properties()
  prop.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.56.11:9091")
  prop.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)
  prop.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getName)

  private val producer: KafkaProducer[String, String] = new KafkaProducer(prop)


  try {
    for (i: Int <- 1 to 10) {
      val record: ProducerRecord[String, String] = new ProducerRecord[String, String]("myTopic", s"Message coming from for loop iteration no: ${i + 1001}")
      producer.send(record,
        (metadata: RecordMetadata, exp: Exception) => {
          // executes everytime a record is successfully sent
          if (exp == null) {
            log.info(
              s"Received new metadata: \n " +
                s"Topic: ${metadata.topic()}\n" +
                s"Partition: ${metadata.partition()}\n" +
                s"offset: ${metadata.offset()}\n" +
                s"Timestamp: ${metadata.timestamp()}\n"
            )
          }
          else {
            log.error(exp.toString)
          }
        }
      )
      try{
        Thread.sleep(1000)
      }
      catch {
        case exp:Exception => log.error(exp.toString)
      }
      producer.flush()
    }

  }
  catch {
    case exp: Exception =>
      println("case exp selected!")
      log.error(s"${exp.toString}")
  }
  finally {
    println("CLOSING THE PARTITIONER")
    producer.close()
  }

}
