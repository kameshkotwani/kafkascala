package learn.wikimedia.project
import org.apache.kafka.clients.producer.KafkaProducer
import com.launchdarkly.eventsource._

import java.net.URI
import java.util.concurrent.TimeUnit

object WikimediaApp extends App {
	val topic = "wikimedia_topic1"
	val url = "https://stream.wikimedia.org/v2/stream/recentchange"
	val wProducer: KafkaProducer[String,String] = new KafkaProducer(myProperties())

	// will allow us to handle the event and send it to Producer
	val eventHandler = new WikimediaHandlerScala(wProducer,topic);

	val eventSource :EventSource  = new EventSource.Builder(eventHandler,URI.create(url)).build()

	// start the producer in another thread
	eventSource.start()

	// we produce for 10 minutes and block the program
	TimeUnit.MINUTES.sleep(1);

}
