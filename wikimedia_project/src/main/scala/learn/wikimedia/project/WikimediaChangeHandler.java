package learn.wikimedia.project;

import com.launchdarkly.eventsource.EventHandler;
import com.launchdarkly.eventsource.MessageEvent;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class WikimediaChangeHandler implements EventHandler{

    KafkaProducer<String,String> producer;
    String topic;
    private final Logger log =  LoggerFactory.getLogger(WikimediaChangeHandler.class.getSimpleName());
    public WikimediaChangeHandler(KafkaProducer<String,String> producer,String topic){
        this.producer = producer;
        this.topic = topic;
    }

    @Override
    public void onOpen() throws Exception {
        // no need to do anything
    }

    @Override
    public void onClosed(){
        try{
            producer.close();
        }
        catch (KafkaException kexp){
            kexp.printStackTrace();
        }
    }

    @Override
    public void onMessage(String event, MessageEvent messageEvent) throws Exception {
        // some async code to send message

        log.info(messageEvent.getData());
        producer.send(new ProducerRecord<>(topic, messageEvent.getData()));

    }

    @Override
    public void onComment(String comment) {
        //nothing
    }

    @Override
    public void onError(Throwable t) {
        log.error("Error in stream reading",t);
    }
}
