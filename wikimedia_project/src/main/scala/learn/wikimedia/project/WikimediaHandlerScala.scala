package learn.wikimedia.project

import com.launchdarkly.eventsource.{EventHandler, MessageEvent}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.KafkaException
import org.slf4j.LoggerFactory

class WikimediaHandlerScala(val producer: KafkaProducer[String, String], val topic: String) extends EventHandler {

	private val log = LoggerFactory.getLogger(classOf[WikimediaHandlerScala].getName)

	override def onOpen(): Unit = log.info("connection opened to passed URI")

	override def onClosed(): Unit = {
		try {
			log.info("Producer closed")
			producer.close()
		}
		catch {
			case kexp: KafkaException => kexp.printStackTrace();
		}
	}


	override def onMessage(event: String, messageEvent: MessageEvent): Unit = {
		log.info(messageEvent.getData)
		producer.send(new ProducerRecord[String, String](topic, messageEvent.getData))
	}

	override def onComment(comment: String): Unit = log.info(comment)

	override def onError(t: Throwable): Unit = log.error("Error in stream reading", t)
}
